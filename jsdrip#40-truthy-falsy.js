//JS Drip #40: Truthy and Falsy Values in JavaScript

var logTruthiness = function(val){
	
	if(val){
		console.log('Truthy');
	} else {
		console.log('Falsy');
	}
}

//Truthy
logTruthiness(true)

logTruthiness('this is string');

logTruthiness({});

logTruthiness([]);

logTruthiness(23.45);

logTruthiness(new Date());

//Falsy
logTruthiness(false);

logTruthiness(null);

logTruthiness(undefined);

logTruthiness(NaN);

logTruthiness(0);

logTruthiness("");