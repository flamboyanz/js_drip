//Using ES6 Rest Parameters to Handle Variable Numbers of Arguments

//es5

function creditStooges(first, second){
    var team = first + " / " + second;

    var theRest = [].slice.call(arguments, 2);

    if(theRest.length > 0){
        team += " / " + theRest.join(" / ")
    }

    console.log(team);
}

creditStooges('joe', 'john', 'doe', 'charles', 'mann');

//es6


function creditStooges(first, second, ...theRest){
    var team = first + " / " + second;

    if(theRest.length > 0){
        team += " / " + theRest.join(" / ")
    }

    console.log(team);
}

creditStooges('joe', 'john', 'doe', 'charles', 'mann');

//es5

function sum(){
    var nums = [].slice.call(arguments)

    return nums.reduce(function(total, current){
        return total + current;
    }, 0);

}
console.log(sum(1, 2, 3))

//es6

function sum(...nums){

    return nums.reduce(function(total, current){
        return total + current;
    }, 0);
}

console.log(sum(1, 2, 3));

