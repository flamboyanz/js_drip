//A Method to the Madness - JavaScript Method Definitions in ES6

//es5

var lex = {

	name: "Lex Luthor",

	takeOverTheWorld: function(){ 
		console.log('take over the world');
	}
}

//ES6

var lex = {
	
	name: "Lex Luthor",
	
	takeOverTheWorld(){ 
		console.log('take over the world');
	}
}

console.log(lex.takeOverTheWorld.name);	//takeOverTheWorld
