//JavaScript's Primitive Wrapper Objects

alert(subh.newtypo) // undefined
alert("something".toUpperCase()) //SOMETHING


var primitive = "hold on hold on"
var subh = new String("dfsd df ds dsf");
subh.newtypo = "hello all";

alert(typeof primitive)       // string
alert(typeof subh)            // object
alert(typeof subh.newtypo)    // string

var prim = "das dasd a sasd";

prim.someFn = function () {
    return typeof this
}

alert(prim.someFn());     // undefined

String.prototype.returnType = function () {
    return typeof this
};

alert(prim.returnType());    // object
