//JS Drip #76: One String to Rule them All - Interpolation with ES6 Template Strings

//es5

function buildListItem(id, classes, content) {
    return  "<li id='" + id + "' classes='" + classes.join(" ") + "'>" +
                content +
            "</li>";
}

var myLi = buildListItem("mine", ["my", "own"], "precious");

console.log(myLi);
// => "<li id='mine' classes='my own'>precious</li>"

//es6

function buildListItem(id, classes, content) {
    var listItem = `
        <li id='${ id }' classes='${ classes.join(" ") }'>
            ${ content }
        </li>`;

    return listItem;
}

var myLi = buildListItem("mine", ["my", "own"], "precious");

console.log(myLi);

/*
<li id='mine' classes='my own'>
     precious
</li>
 */