//Storing Metadata on Arrays in JavaScript

var digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

function filterDigits(filterFn) {
    return {
        filtered: digits.filter(filterFn),
        timeStamp: new Date()
    }
}

var filterObj = filterDigits(
    function (x) {
        return ( x > 8 );
    })

alert(filterObj.filtered);    //9
alert(filterObj.timeStamp);    // Nov17...

// Optimized approach

var digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

function filterDigits(filterFn) {
    var result = digits.filter(filterFn);
    result.timeStamp = new Date();
    return result;
}

var filtered = filterDigits(function (x) {
    return (x > 8)
})

alert(filtered.timeStamp); // Nov17..