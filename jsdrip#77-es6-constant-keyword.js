"use strict";

var ACQUISITION_RATE = 0.0007;

 function oddsOfSuperPowers (personalEnergy, eventEnergy) {
 return personalEnergy * eventEnergy * ACQUISITION_RATE;
 }

 ACQUISITION_RATE = ACQUISITION_RATE * 23;

 console.log(oddsOfSuperPowers(1, 3));

const ACQUISITION_RATE = 0.0007;

function oddsOfSuperPowers(personalEnergy, eventEnergy) {
    return personalEnergy * eventEnergy * ACQUISITION_RATE;
}

ACQUISITION_RATE = ACQUISITION_RATE * 23;

console.log(oddsOfSuperPowers(1, 3));

// Any constant declared by const cannot be reassigned.
// it also follows that they must be initialized with a value.

const noValue;

// const does not make objects or arrays immutable.

const rogue = {name: "Anna Marie"};

rogue.power = "Absorption";

console.log(rogue);
// => { name: "Anna Marie", power: "Absorption" }

const rogue = {name: "Jean Grey"};
// => TypeError: redeclaration of const rogue

//If you need to make an object immutable, you'll need to freeze it.

//Unlike var the const keyword is block-scoped, not function-scoped.
if (true) {
    var variable = "variable";
    const constant = "constant";
}

console.log(variable);
// => "variable"

console.log(constant);
// => ReferenceError: constant is not defined
