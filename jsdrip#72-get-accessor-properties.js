//Get Set with Accessor Properties in JavaScript

// object literal way

var circle = {
    radius: 2,
    getDiameter : function(){
        return this.radius * 2;
    }
}
console.log(circle.getDiameter());

// getter

var circle = {
    radius: 2,
    get diameter(){
        return this.radius * 2;
    }
}
console.log(circle.diameter);

// getter and setter

var circle = {
    radius: 2,
    get diameter(){
        return this.radius * 2;
    },

    set diameter(val){
        this.radius = val / 2
    }
}
console.log(circle.diameter);
circle.diameter = 32;
console.log(circle.radius);

// object.create or object define property

var circle = Object.create(Object, {

    radius: {
        value: 2,
        writable: true
    },

    diameter: {
        get: function(){
            return this.radius;
        },
        set: function(val){
            this.radius = val / 2;
        }
    }
});

console.log(circle.diameter)
circle.diameter = 45;
console.log(circle.radius)