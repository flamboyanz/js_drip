//Hiding in Plain Sight - Enumerability in JavaScript

var fantasticFour = {
    mrFantastic: "Reed Richards",
    invisibleWoman: "Susan Storm",
    humanTorch: "Johnny Storm",
    thing: "Ben Grimm"
};

Object.defineProperty(fantasticFour, 'invisibleWoman', {
    enumerable: false
});

var listVisible = function (team) {
    var list = [];
    for (var member in team) {
        list.push(team[member])
    }
    ;
    return list;
};

console.log(listVisible(fantasticFour))

console.log(Object.keys(fantasticFour));

console.log('invisibleWoman' in fantasticFour);

console.log(fantasticFour.hasOwnProperty('invisibleWoman'));

console.log(Object.getOwnPropertyNames(fantasticFour));

console.log(fantasticFour.propertyIsEnumerable('invisibleWoman'));

console.log(fantasticFour.invisibleWoman);

var configuration = Object.getOwnPropertyDescriptor(fantasticFour, 'invisibleWoman');
console.log(configuration);
console.log(configuration.enumerable);
console.log(configuration.configurable);
console.log(configuration.writable);
console.log(configuration.value);