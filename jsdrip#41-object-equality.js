//Object Equality in JavaScript

var danny = {"big": "some else", "small": "thing else"};

var sunny = {"big": "some else", "small": "thing else"};

var bobby = sunny;
//alert(sunny === danny)    //false
//alert(bobby === sunny)    //true

function isEquivalent(a, b) {
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    if (aProps.length !== bProps.length) return false;

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];
        if (a[aProps] !== b[bProps]) return false;
    }
    return true;
}

alert(isEquivalent(danny, sunny));