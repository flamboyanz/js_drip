//JS Drip #70: Even Stricter Equality with Object.is

console.log(NaN === NaN);   //false

console.log(0 === -0);  //true

function isNaNValue(va){
    return val !== val;
}

console.log(isNaNValue(NaN));   //true

function isNegativeZero(val){
    if(val !== 0) return;
    var thisInfinity = 1 / val;
    return (thisInfinity < 0)
}
isNegativeZero(-0);  //true


Object.is(NaN, NaN); //true

Object.is(String(), String()); //true

Object.is(window, window)   //true

Object.is(window.document, window)  //false